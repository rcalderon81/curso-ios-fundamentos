//
//  main.m
//  Tarea04
//
//  Created by Deidania Barahona Orozco on 12/9/15.
//  Copyright © 2015 Rodrigo Calderon Araya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
